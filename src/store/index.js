import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    profile: [
      {
        id: 1,
        name: 'Eco-60',
        price: 500,
      },
      {
        id: 2,
        name: 'Eco-40',
        price: 820,
      },
      {
        id: 1,
        name: 'Eco-80',
        price: 650,
      },
      {
        id: 1,
        name: 'Pro-10',
        price: 1000,
      },
    ],
    glassUnit: [
      {
        id: 1,
        name: 'Двухкамерный стеклопакет',
        price: 3000,
        width: 1300,
        height: 1750
      },
      {
        id: 2,
        name: 'Однокамерный энергосберегаюший стеклопакет',
        price: 4000,
        width: 650,
        height: 1750
      },
      {
        id: 3 ,
        name: 'Однокамерный стеклопакет с использованием триплекса',
        price: 5000,
        width: 650,
        height: 1750
      },
      {
        id: 4,
        name: 'Антирезонансный шумозашитный  стеклопакет',
        price: 6000,
        width: 650,
        height: 1750
      },
      {
        id: 5,
        name: 'Двухкамерный энергосберегаюший  стеклопакет',
        price: 6500,
        width: 130,
        height: 1750
      }
    ],
    colors: [
      {
        id: 1,
        name: 'Белый'
      },
      {
        id: 2,
        name: 'Черный'
      },
      {
        id: 3,
        name: 'Коричневый'
      },
    ],
    windowType: [
      {
        id: 1,
        name: 'Поворотное',
        price: 400
      },
      {
        id: 2,
        name: 'Глухое',
        price: 300
      },
      {
        id: 3,
        name: 'Откидное',
        price: 500
      },
      {
        id: 4,
        name: 'Поворотное-откидное',
        price: 800
      },
    ],
  },
  mutations: {

  },
  actions: {

  },
  modules: {

  },
  getters: {
      getWindowProfile: (state) => {
        return state.profile
      },
      getWindowGlassUnit: (state) => {
        return state.glassUnit
      },
      getWindowColor: (state) => {
        return state.colors
      },
      getWindowType: (state) => {
        return state.windowType
      }
  }
})
