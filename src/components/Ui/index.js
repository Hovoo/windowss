import Vue from 'vue'

Vue.component('UiButton', require('./vButton').default)
Vue.component('UiInput', require('./vInput').default)
Vue.component('UiSelect', require('./vSelect').default)
